//
// Created by Dmytrii Spyrydonov on 2/10/18.
//

#ifndef GAM_OF_LIFE_WORLD_H
#define GAM_OF_LIFE_WORLD_H

#define HEIGHT 50 //50 rows
#define WIDTH 50 //50 lines
#define SLEEP 500000 //set to 0.5 seconds per iteration

#include <iostream>
#include <unistd.h>
#include <iomanip>

using namespace std;

class World {
public:
    World();
    bool    iterate(string n);
    bool    turnlive(string str);
    void    print();
    bool    parse(string str);

private:
    //variables:
    char    map[HEIGHT][WIDTH];
    char    alt_map[HEIGHT][WIDTH];
    int     x_cor;
    int     y_cor;
    bool    toggle;
    //methods:
    void    checkmap();
    bool    is_number(const string& s);
    void    copyAltMap();
    bool    setFigure(string str);
    bool    setGlider();
    bool    setExploder();
    bool    setRow();
    bool    setSpaceship();
    bool    setTumbler();
    bool    checkCor(string cor);
};


#endif //GAM_OF_LIFE_WORLD_H
