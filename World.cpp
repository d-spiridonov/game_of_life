//
// Created by Dmytrii Spyrydonov on 2/15/18.
//

#include "World.h"


World::World() {
    for (int i = 0; i < HEIGHT ; i++){
        for(int j = 0; j < WIDTH; j++){
            map[i][j] = '.';
        }
    }
    toggle = true;
}

//assigning the user's input to variables
bool World::checkCor(string cor){
    int i = 0;
    x_cor = 0;
    y_cor = 0;
    while (isdigit(cor[i])) {
        y_cor = y_cor * 10 + cor[i++] - 48;
    }
    while (isdigit(cor[++i]))
        x_cor = x_cor * 10 + cor[i] - 48;
    return cor.length() == i;
}

//selecting the right figure
bool World::setFigure(string str) {
    string cor;
    int i = 0;

    cout << "Enter the column and row of a cell where you want to place the figure"\
    " separated by space (example: 1 1)" << endl;
    while (!checkCor(cor)) {
        if (i++ > 0)
            cout << "Incorrect input" << endl;
        getline(cin, cor);
    }
    if (str == "Glider")
        return setGlider();
    else if (str == "Exploder")
        return setExploder();
    else if (str == "Row")
        return setRow();
    else if (str == "Spaceship")
        return setSpaceship();
    else if (str == "Tumbler") {
        return setTumbler();
    }
    else {
        cout << "Incorrect input" << endl;
        return false;
    }
}

//setting the tumbler to the map
bool World::setTumbler() {
    int x_map = x_cor;

    char spaceship[6][7] = {
            {'.', 'X', 'X', '.', 'X', 'X', '.'},
            {'.', 'X', 'X', '.', 'X', 'X', '.'},
            {'.', '.', 'X', '.', 'X', '.', '.'},
            {'X', '.', 'X', '.', 'X', '.', 'X'},
            {'X', '.', 'X', '.', 'X', '.', 'X'},
            {'X', 'X', '.', '.', '.', 'X', 'X'},
    };
    if (y_cor + 6 > HEIGHT || x_cor + 7 > WIDTH){
        cout << "Coordinates are out of range. Enter coordinates between 0 - " << HEIGHT << "and 0 - " << WIDTH << endl;
        return false;
    }
    for (int y = 0; y <  6; y++){
        for (int x = 0; x < 7; x++){
            map[y_cor][x_map++] = spaceship[y][x];
        }
        x_map = x_cor;
        y_cor++;
    }
    return true;
}

//setting the spaceship to the map
bool World::setSpaceship() {
    int x_map = x_cor;

    char spaceship[4][5] = {
            {'.', 'X', 'X', 'X', 'X'},
            {'X', '.', '.', '.', 'X'},
            {'.', '.', '.', '.', 'X'},
            {'X', '.', '.', 'X', '.'}
    };
    if (y_cor+ 4 > HEIGHT || x_cor + 5 > WIDTH){
        cout << "Coordinates are out of range. Enter coordinates between 0 - " << HEIGHT << "and 0 - " << WIDTH << endl;
        return false;
    }
    for (int y = 0; y < 4; y++){
        for (int x = 0; x < 5; x++){
            map[y_cor][x_map++] = spaceship[y][x];
        }
        x_map = x_cor;
        y_cor++;
    }
    return true;
}

//setting the row figure to the map
bool World::setRow() {
    char row[10] = {'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'};

    if (y_cor > HEIGHT || y_cor + 10 > WIDTH){
        cout << "Coordinates are out of range. Enter coordinates between 0 - " << HEIGHT << "and 0 - " << WIDTH << endl;
        return false;
    }
    for (int x = 0; x < 10; x++){
        map[y_cor][x_cor++] = row[x];
    }
    return true;
}

//setting the Glider figure to the map
bool World::setGlider() {
    int x_map = x_cor;

    char glider[3][3] = {
            {'.', 'X', '.'},
            {'.', '.', 'X'},
            {'X', 'X', 'X'}
    };

    if (y_cor + 3 > HEIGHT || x_cor + 3 > WIDTH){
        cout << "Coordinates are out of range. Enter coordinates between 0 - " << HEIGHT << "and 0 - " << WIDTH << endl;
        return false;
    }
    for (int y = 0; y < 3; y++){
        for (int x = 0; x < 3; x++){
                map[y_cor][x_map++] = glider[y][x];
        }
        x_map = x_cor;
        y_cor++;
    }
    return true;
}

//setting the exploder figure to the map
bool World::setExploder() {
    int x_map = x_cor;

    char exploder[5][5] = {
            {'X', '.', 'X', '.', 'X'},
            {'X', '.', '.', '.', 'X'},
            {'X', '.', '.', '.', 'X'},
            {'X', '.', '.', '.', 'X'},
            {'X', '.', 'X', '.', 'X'},
    };
    if (y_cor + 5 > HEIGHT || x_cor + 5 > WIDTH){
        cout << "Coordinates are out of range. Enter coordinates between 0 - " << HEIGHT << "and 0 - " << WIDTH << endl;
        return false;
    }
    for (int y = 0; y < 5; y++){
        for (int x = 0; x < 5; x++){
            map[y_cor][x_map++] = exploder[y][x];
        }
        x_map = x_cor;
        y_cor++;
    }
    return true;
}

//parsing the user's input and selecting the appropriate method for processing
bool World::parse(string str) {
    if (checkCor(str))
        return turnlive(str);
    else if (str == "Glider" || str == "Exploder" || str == "Row" || str == "Spaceship"\
    || str == "Tumbler"){
        return setFigure(str);
    }
    else {
        cout << "Incorrect input" << endl;
        return false;
    }
}

//turning the cell into "Live" state
bool World::turnlive(string str) {

    if (x_cor > HEIGHT || y_cor > WIDTH){
        cout << "Error: Out of map range" << endl;
        return false;
    }
         if (map[x_cor][y_cor] != 'X') {
             map[x_cor][y_cor] = 'X';
             return true;
         }
         else {
             cout << "Already live" << endl;
             return false;
         }
}

//printing out the map
void World::print() {
    cout << setw(4) << " ";
    for (int i = 0; i < WIDTH; i++){
        if (i % 5 == 0 && i < 10)
            cout << i;
        else if (i > 9 && i % 5 == 0){
            cout << i++;
        }
        else
            cout << " ";
    }
    cout << endl;
    for (int i = 0; i < HEIGHT; i++) {
        cout << setw(4) << i;
        for (int j = 0; j < WIDTH; j++) {
            cout << map[i][j];
        }
        cout << endl;
    }
}

//checking if the string is a number
bool World::is_number(const string& s)
{
    return !s.empty() && std::find_if(s.begin(), \
    s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}

//iterating the world
bool World::iterate(string n) {
    int num;

    if (!is_number(n)){
        cout << "Incorrect input. Type a number" << endl;
        return false;
    }
    else {
        num = stoi(n);
        for (int i = 0; i < num; i++) {
            checkmap();
            print();
            usleep(SLEEP);
        }
    }
    return true;
}

//copying the map into a temporary one while iterating the world 1 step forward
void World::copyAltMap() {
    if (toggle){
        for (int j = 0; j < HEIGHT; j++){
            for (int k = 0; k < WIDTH; k++){
                alt_map[j][k] = map[j][k];
            }
        }
        toggle = false;
    }
    else{
        for (int j = 0; j < HEIGHT; j++){
            for (int k = 0; k < WIDTH; k++){
                map[j][k] = alt_map[j][k];
            }
        }
        toggle = true;
    }
}

//checking the positions of live cells on the map and applying the Game of Live logic
void World::checkmap() {
    int count; int i;

    copyAltMap();
    for (int y = 0; y < HEIGHT; y++){
        for (int x = 0; x < WIDTH; x++){
           count = 0;
           i = y - 2;
            while (i++ < y + 1) {
                if (i != -1) {
                    if (i != y && map[i][x] == 'X') {
                        count++;
                    }
                    if (x != 0) {
                        if (map[i][x - 1] == 'X') {
                            count++;
                        }
                    }
                    if (x != WIDTH) {
                        if (map[i][x + 1] == 'X') {
                            count++;
                        }
                    }
                }
                if (y == HEIGHT && i == y)
                    break;
            }
            if (map[y][x] == '.' && count == 3)
                alt_map[y][x] = 'X';
            else if (map[y][x] == 'X' && (count < 2 || count > 3)){
                alt_map[y][x] = '.';
            }
        }
    }
    copyAltMap();//copying the changes into the original map
}