
#include <iostream>
#include "World.h"

using namespace std;


int main(){

    string n;

    World world;
    cout << left << "Welcome to Conway's Game of Life." << endl << endl;
    cout << "The Rules of Life:" << endl;
    cout << "1. Any live cell with fewer than two live neighbors dies, as if by loneliness." << endl;
    cout << "2. Any live cell with more than three live neighbors dies, as if by \novercrowding." << endl;
    cout << "3. Any live cell with two or three live neighbors lives, unchanged." << endl;
    cout << "4. Any dead cell with exactly three live neighbors comes to life." << endl << endl;
    cout << "To play: Press any key to begin. Enter the column and row of a cell to make it \nalive, separated by a space. ";
    cout << "When you are ready, enter \"NEXT\" to begin the \nsimulation. Then type \"RETURN\" to go to the previous menu "\
    "or type \"STOP\" to quit." << endl;

    getline(cin, n);
    while(n != "STOP"){
        cout << "Enter the column and row of a cell to make it alive, separated by a space, \n type the name of the figure "\
    "(Glider/Exploder/Row/Spaceship/Tumbler) \n or type \"NEXT\" to move to the next step." << endl;
        while (getline(cin, n) && n != "NEXT" && n != "STOP"){
                if (world.parse(n)) {
                    world.print();
                }
            cout << "Enter the column and row of a cell to make it alive, separated by a space, \ntype the name of the figure "\
    "(Glider/Exploder/Row/Spaceship/Tumbler) \nor type \"NEXT\" to move to the next step." << endl;
        }
        if (n == "STOP")
            break;
        cout << "Enter the number of iterations to be processedor type \"RETURN\" to return to "\
            "the previous menu" << endl;
        getline(cin, n);
        while (n != "RETURN" && n != "STOP" && world.iterate(n)){
            cout << "Enter the number of iterations to be processed to continue iterating or type \"RETURN\" to return to "\
            "the previous menu" << endl;
            getline(cin, n);
        }
    }
}